package c.com.app.fatoorah.adminpanel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AdminLoginActivity extends AppCompatActivity {





    private Button btnLogin;
    private Button btnSignUp;

    private EditText etUserEmail;
    private EditText etPassword;





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnSignUp = (Button) findViewById(R.id.btn_signup);

        etUserEmail = (EditText) findViewById(R.id.et_username_email);
        etPassword = (EditText) findViewById(R.id.et_password);

    }





    /** Button click event for SIGN-UP button. */
    public void register(View v) {

        // If successfully registered, it will reset fields and leaves with a toast message.
        etUserEmail.setText(null);
        etPassword.setText(null);

        Toast.makeText(AdminLoginActivity.this, "Successfully registered as admin!", Toast.LENGTH_SHORT).show();

    }





    /** Button click event for LOG-IN button. */
    public void signIn(View v) {

        // If account name and password are correct, entering admin menu...
        // This is TEST MODE. Replace with IF-ELSE and auth here.
        startActivity(new Intent(AdminLoginActivity.this, AdminMenu.class));

    }





}
