package c.com.app.fatoorah.adminpanel.logrequest;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.logrequest.activitylogview.LogContent;
import c.com.app.fatoorah.adminpanel.logrequest.activitylogview.LogContent.LogContentItem;

public class AccountRequestListFragment extends Fragment {





    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;





    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AccountRequestListFragment() {

    }





    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static AccountRequestListFragment newInstance(int columnCount) {

        AccountRequestListFragment fragment = new AccountRequestListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);

        }

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.account_request_fragment_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {

            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            if (mColumnCount <= 1) {

                recyclerView.setLayoutManager(new LinearLayoutManager(context));

            } else {

                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

            }

            AccountRequestUserRecyclerViewAdapter adapter = new AccountRequestUserRecyclerViewAdapter(LogContent.ITEMS, mListener);
            recyclerView.setAdapter(adapter);

        }

        return view;

    }





    /** TODO: TEMPORARILY DISABLED DUE TO ERRORS. IT WILL WORK JUST FINE. */
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }





    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(LogContentItem item);

    }





}
