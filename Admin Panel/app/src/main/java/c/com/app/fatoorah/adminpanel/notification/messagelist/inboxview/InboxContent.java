package c.com.app.fatoorah.adminpanel.notification.messagelist.inboxview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InboxContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<InboxItem> ITEMS = new ArrayList<InboxItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, InboxItem> ITEM_MAP = new HashMap<String, InboxItem>();

    private static final int COUNT = 5; // TODO: --> Dummy email count.





    /** Test generate */
    static {

        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {

            addItem(createDummyInboxItem(i));

        }

    }





    private static void addItem(InboxItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }





    // TODO: Need to generate with real user info (email) instead of this dummy generator.
    private static InboxItem createDummyInboxItem(int position) {

        return new InboxItem("email" + String.valueOf(position) + "@crapmail.com", "MESSAGE TITLE", makeDetails(position));

    }





    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }





    public static class InboxItem {

        // TODO: Revise variable content. (see comments)

        // Used for testing
        public final String id;
        public final String content;
        public final String details;

        // Real deal
//        public final String email;
//        public final String topic; // --> The message's title



        /** Generate for testing item list display */
        public InboxItem(String id, String content, String details) {

            this.id = id;
            this.content = content;
            this.details = details;

        }

        /** Set the email address and title of the user for messaging. */
//        public InboxItem(String email, String topic) {
//
//            this.email = email;
//            this.topic = topic;
//
//        }

        @Override
        public String toString() {
            return content; // --> Replace "content" with "email."
        }

    }





}
