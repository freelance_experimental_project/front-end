package c.com.app.fatoorah.adminpanel.logrequest;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.logrequest.AccountRequestListFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.adminpanel.logrequest.activitylogview.LogContent.LogContentItem;
import c.com.app.fatoorah.adminpanel.logrequest.dialog.LogDialog;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class AccountRequestUserRecyclerViewAdapter extends RecyclerView.Adapter<AccountRequestUserRecyclerViewAdapter.ViewHolder> {





    private final List<LogContentItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public AccountRequestUserRecyclerViewAdapter(List<LogContentItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_request_fragment_user, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // TODO: Revise variable name according to "LogContentItem" class.

        // Display some contents of an item per list here.
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).email); // --> i.g. mTvTopic should be mEmail

        // TODO: For the dialog...
        final String id = mValues.get(position).id;
        final String email = mValues.get(position).email;
        final String action = mValues.get(position).action;
        final String dateAndTime = mValues.get(position).date + ", " + mValues.get(position).time;
        final String platform = mValues.get(position).platform;

        // Show all content of a selected content here.
        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }

                // Dialog
                FragmentManager fm = ((FragmentActivity)v.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new LogDialog();

                // TODO: Display/Revise all necessary contents from the user who signed-up or logged in for the dialog.
                ((LogDialog) dialog).message = "ID: " + id +
                                                "\nEmail" + email +
                                                "\nAction: " + action +
                                                "\nDate and Time: " + dateAndTime +
                                                "\nPlatform: " + platform;

                dialog.show(fm, "LOG CONTENT DIALOG");

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        // TODO: Revise variable name according to "LogContentItem" class.
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public LogContentItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number_account);
            mContentView = (TextView) view.findViewById(R.id.content_account);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }

    }





}
