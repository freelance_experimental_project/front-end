package c.com.app.fatoorah.adminpanel.receiptmanagement.approval;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.receiptmanagement.approval.ReceiptApprovalFragmentActivity.OnListFragmentInteractionListener;
import c.com.app.fatoorah.adminpanel.receiptmanagement.approval.approvalview.DummyContent.DummyItem;
import c.com.app.fatoorah.adminpanel.receiptmanagement.dialog.ReceiptPreviewDialog;

import java.util.List;

/** Recycler view adapter for loading all of users' pending approval requests. */
public class ApprovalItemRecyclerViewAdapter extends RecyclerView.Adapter<ApprovalItemRecyclerViewAdapter.ViewHolder> {





    private final List<DummyItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public ApprovalItemRecyclerViewAdapter(List<DummyItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.receipt_approval_fragment_item, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // TODO: Revise variable name after replacing DummyItem class in onBindViewHolder().
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }

                // Dialog (TODO: Real image URL for receipt is required. Displayed with dummy image instead)
                FragmentManager fm = ((FragmentActivity)v.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new ReceiptPreviewDialog();
//                ((ReceiptPreviewDialog) dialog).imageResource = "? ? ?";
                dialog.show(fm, "APPROVAL RECEIPT DIALOG");

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        // TODO: Revise variable name after renaming/replacing DummyItem accordingly.
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ImageView mImgReceipt;
        public DummyItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number_receipt_approval);
            mContentView = (TextView) view.findViewById(R.id.content_receipt_approval);
            mImgReceipt = (ImageView) view.findViewById(R.id.image_receipt);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }

    }





}
