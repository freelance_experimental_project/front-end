package c.com.app.fatoorah.adminpanel.receiptmanagement.uploadhistory.receiptview;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Item view content, shows the user's receipt and date when it uploaded. */
public class ReceiptViewContent {





    /**
     * An array of sample uploaded receipt items.
     */
    public static final List<ReceiptItem> ITEMS = new ArrayList<ReceiptItem>();

    /**
     * A map of sample uploaded receipt items, by ID.
     */
    public static final Map<String, ReceiptItem> ITEM_MAP = new HashMap<String, ReceiptItem>();

    private static final int COUNT = 24;





    /** TODO: Test item display. Revise it. */
    static {

        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {

            addItem(createDummyItem(i));

        }

    }





    private static void addItem(ReceiptItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }





    private static ReceiptItem createDummyItem(int position) {

        return new ReceiptItem(String.valueOf(position), "Upload date: " + String.valueOf(new Date().getTime()), makeDetails(position));

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }

        return builder.toString();

    }





    /**
     * Receipt item representing a piece of content.
     *
     * <br/>
     * TODO: Revision required.
     */
    public static class ReceiptItem {

        public final String id;
        public final String content; // --> Can be date uploaded.
        public final String details;
        public final String imgUrl; // --> This needed.

        public ReceiptItem(String id, String content, String details) {

            this.id = id;
            this.content = content;
            this.details = details;
            imgUrl = "null"; // --> empty for now due to testing

        }

//        public ReceiptItem(String id, String content, String imgUrl) {
//
//            this.id = id;
//            this.content = content;
//            this.details = "empty";
//            this.imgUrl = imgUrl;
//
//        }

        @Override
        public String toString() {
            return content;
        }

    }





}
