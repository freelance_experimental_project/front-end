package c.com.app.fatoorah.adminpanel.notification.messagelist;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.notification.dialog.InboxOptionDialog;
import c.com.app.fatoorah.adminpanel.notification.messagelist.InboxFragmentActivity.OnListFragmentInteractionListener;
import c.com.app.fatoorah.adminpanel.notification.messagelist.inboxview.InboxContent.InboxItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link InboxItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class InboxRecyclerViewAdapter extends RecyclerView.Adapter<InboxRecyclerViewAdapter.ViewHolder> {





    private final List<InboxItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public InboxRecyclerViewAdapter(List<InboxItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inbox_fragment_item, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // TODO: Revise variable from test content to real content. (InboxContent.class) See comments.
        holder.mItem = mValues.get(position);
        holder.mTvEmail.setText(mValues.get(position).id); // --> "id" to "email"
        holder.mTvTopic.setText(mValues.get(position).content); // --> "content" to "topic"

        final String email = mValues.get(position).id; // --> Still...change "id" to "email"

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }

                // Dialog
                FragmentManager fm = ((FragmentActivity)v.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new InboxOptionDialog();
                ((InboxOptionDialog) dialog).email = email;
                dialog.show(fm, "INBOX OPTION DIALOG");

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mTvEmail;
        public final TextView mTvTopic;
        public InboxItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mTvEmail = (TextView) view.findViewById(R.id.tv_email_address_inbox);
            mTvTopic = (TextView) view.findViewById(R.id.tv_topic_inbox);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTvTopic.getText() + "'";
        }

    }





}
