package c.com.app.fatoorah.adminpanel.cashbackhistory.cashbackview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** A database contains contents related to cashback history */
public class CashbackContent {

    public static final List<CashbackItem> ITEMS = new ArrayList<CashbackItem>();
    public static final Map<String, CashbackItem> ITEM_MAP = new HashMap<String, CashbackItem>();

    private static final int COUNT = 25;





    // TODO: Revise fromtest mode to official content load.
    static {

        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {

            addItem(createDummyItem(i)); // --> Revise this from a hard-coded "createDummyItem" to a legit ones.

        }

    }





    private static void addItem(CashbackItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }





    private static CashbackItem createDummyItem(int position) {

        return new CashbackItem("dummy" + position + "@yahoo.com", "Price return: $" + 999, makeDetails(position));

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }

        return builder.toString();

    }





    /** A cashback history item representing a piece of content. */
    public static class CashbackItem {

        // TODO: Revised it. This content was arranged for testing. See marked comments.

        public final String id;
//        public final String content;
        public final String details; // --> comment out or remove it.

        // Real content item
        public final String email;
        public final String rebatePrice = "null"; // --> Disabled for now due to testing phase. Revise it.

        // Used in testing.
        public CashbackItem(String id, String email, String details) {

            this.id = id;
            this.email = email;
            this.details = details;

        }

        // Real constructor (DISABLED)
//        public CashbackItem(String id, String email, String rebatePrice) {
//
//            this.id = id;
//            this.email = email;
//            this.rebatePrice = rebatePrice;
//
//        }

        @Override
        public String toString() {
            return email;
        }

    }





}
