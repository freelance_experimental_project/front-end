package c.com.app.fatoorah.adminpanel.manageusers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.manageusers.AccountEditListFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.adminpanel.manageusers.userview.EditUserContent.UserContentItem;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class AccountEditUserRecyclerViewAdapter extends RecyclerView.Adapter<AccountEditUserRecyclerViewAdapter.ViewHolder> {





    private final List<UserContentItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public AccountEditUserRecyclerViewAdapter(List<UserContentItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_edit_fragment_user, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // TODO: Revise variable name according to "UserContentItem" class.

        // Display some contents of an item per list here.
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).email); // --> Should be named "mEmail" instead of "mTvTopic" for example

        // Show all content of a selected content here.
        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        // TODO: Revise variable name according to "UserContentItem" class.
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public UserContentItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number_account_edit);
            mContentView = (TextView) view.findViewById(R.id.content_account_edit);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }

    }





}
