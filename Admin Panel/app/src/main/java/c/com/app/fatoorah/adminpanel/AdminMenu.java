package c.com.app.fatoorah.adminpanel;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import c.com.app.fatoorah.adminpanel.cashbackhistory.CashbackHistoryFragmentActivity;
import c.com.app.fatoorah.adminpanel.logrequest.AccountRequestListFragment;
import c.com.app.fatoorah.adminpanel.manageusers.AccountEditListFragment;
import c.com.app.fatoorah.adminpanel.notification.NotificationManagementMessageFragActivity;
import c.com.app.fatoorah.adminpanel.notification.dialog.InboxOptionDialog;
import c.com.app.fatoorah.adminpanel.notification.messagelist.InboxFragmentActivity;
import c.com.app.fatoorah.adminpanel.offer.OfferItemFragmentActivity;
import c.com.app.fatoorah.adminpanel.receiptmanagement.approval.ReceiptApprovalFragmentActivity;
import c.com.app.fatoorah.adminpanel.receiptmanagement.uploadhistory.UploadedReceiptFragActivity;
import c.com.app.fatoorah.adminpanel.ui.toolfragment.BlankFragment;
import c.com.app.fatoorah.adminpanel.ui.toolfragment.EditButtonsFragment;

public class AdminMenu extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {





    // Fragment for Content
    private Fragment fragment;
    private FragmentTransaction ft;
    private FragmentManager fm;

    // Fragment for Search from List Edit Text
    private FragmentTransaction ft2;
    private FragmentManager fm2;

    // Fragment for list item buttons (managing purposes)
    private FragmentTransaction ft3;
    private FragmentManager fm3;





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fm = getSupportFragmentManager();
        fm2 = getSupportFragmentManager();
        fm3 = getSupportFragmentManager();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }

        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }





    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {

            drawer.closeDrawer(GravityCompat.START);

        } else {

            super.onBackPressed();

        }

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_menu, menu);
        return true;

    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // Always empty search text from Notification Messenger first when admin is selecting options.
        InboxOptionDialog.emptySearchTextboxFromMessenger();

        if(id == R.id.nav_login_signup_request) {

            Toast.makeText(AdminMenu.this, "SIGN-UP REQUEST", Toast.LENGTH_SHORT).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new SearchFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new AccountRequestListFragment();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new BlankFragment());
            ft3.commit();

        } else if(id == R.id.nav_add_edit_delete_users) {

            Toast.makeText(AdminMenu.this, "EDIT USERS", Toast.LENGTH_SHORT).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new SearchFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new AccountEditListFragment();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new EditButtonsFragment());
            ft3.commit();

        } else if(id == R.id.nav_cashback_history) {

            Toast.makeText(AdminMenu.this, "USER'S CASHBACK HISTORY", Toast.LENGTH_SHORT).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new SearchFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new CashbackHistoryFragmentActivity();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new BlankFragment());
            ft3.commit();

        } else if(id == R.id.nav_manage_notififcation) {

            Toast.makeText(AdminMenu.this, "MANAGE NOTIFICATION", Toast.LENGTH_SHORT).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new SearchFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new InboxFragmentActivity();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new EditButtonsFragment());
            ft3.commit();

        } else if(id == R.id.nav_send_notififcation) {

            Toast.makeText(AdminMenu.this, "SEND NOTIFICATION", Toast.LENGTH_SHORT).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new BlankFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new NotificationManagementMessageFragActivity();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new BlankFragment());
            ft3.commit();

        } else if(id == R.id.nav_add_update_delete_offer) {

            Toast.makeText(AdminMenu.this, "EDIT OFFER", Toast.LENGTH_SHORT).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new SearchFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new OfferItemFragmentActivity();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new EditButtonsFragment());
            ft3.commit();

        } else if(id == R.id.nav_receive_receipt) {

            Toast.makeText(AdminMenu.this, "RECEIVE RECEIPT\nTap the receipt to zoom.", Toast.LENGTH_SHORT).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new SearchFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new UploadedReceiptFragActivity();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new BlankFragment());
            ft3.commit();

        } else if(id == R.id.nav_approve_reject_receipt) {

            Toast.makeText(AdminMenu.this, "APPROVE OR REJECT RECEIPT.\nTap the receipt to zoom.", Toast.LENGTH_LONG).show();

            // Search Frag
            ft2 = fm2.beginTransaction();
            ft2.replace(R.id.fragment_search, new SearchFragment());
            ft2.commit();

            // Selected Frag
            ft = fm.beginTransaction();
            fragment = new ReceiptApprovalFragmentActivity();
            ft.replace(R.id.fragment_content, fragment);
            ft.commit();

            // Edit Frag
            ft3 = fm3.beginTransaction();
            ft3.replace(R.id.fragment_option_buttons, new BlankFragment());
            ft3.commit();

        } else {

            Toast.makeText(AdminMenu.this, "LOGGING OUT...", Toast.LENGTH_LONG).show();

            // Add events for account authentication for logging out admin before exit.
            finish();

        }

//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }





    /** Test click listener from the test fragment's red button. This closes app. */
    public void testClickFragment(View v) {

        finish();

    }





}
