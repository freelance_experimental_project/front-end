package c.com.app.fatoorah.adminpanel.receiptmanagement.uploadhistory;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.receiptmanagement.dialog.ReceiptPreviewDialog;
import c.com.app.fatoorah.adminpanel.receiptmanagement.uploadhistory.UploadedReceiptFragActivity.OnListFragmentInteractionListener;
import c.com.app.fatoorah.adminpanel.receiptmanagement.uploadhistory.receiptview.ReceiptViewContent.ReceiptItem;

import java.util.List;

/** Recycler class that loads a history of uploaded receipts. */
public class UploadedReceiptItemRecyclerViewAdapter extends RecyclerView.Adapter<UploadedReceiptItemRecyclerViewAdapter.ViewHolder> {





    private final List<ReceiptItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public UploadedReceiptItemRecyclerViewAdapter(List<ReceiptItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.uploaded_receipt_fragment_item, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // TODO: Revise variable name according to "ReceiptItem" class.
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }

                // Dialog (TODO: Real image URL for receipt is required. Displayed with dummy image instead)
                FragmentManager fm = ((FragmentActivity)v.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new ReceiptPreviewDialog();
//                ((ReceiptPreviewDialog) dialog).imageResource = "? ? ?";
                dialog.show(fm, "RECEIPT DIALOG");

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // TODO: Revise variable name according to "ReceiptItem" class.
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ImageView mImgReceipt; // --> TODO: Do something to display receipt.

        public ReceiptItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number_upload_history);
            mContentView = (TextView) view.findViewById(R.id.content_upload_history);
            mImgReceipt = (ImageView) view.findViewById(R.id.image_receipt);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }

    }





}
