package c.com.app.fatoorah.adminpanel.notification.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.notification.NotificationManagementMessageFragActivity;
import c.com.app.fatoorah.adminpanel.ui.toolfragment.BlankFragment;

public class InboxOptionDialog extends DialogFragment {





    public String email;
    private static String emailAddress;





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Send new message to " + email + "?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        // Redirecting to Messenger fragment...
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_content, new NotificationManagementMessageFragActivity());
                        ft.commit();

                        // Hiding the search text and multiple edit button fragment...
                        FragmentTransaction ft2 = getActivity().getSupportFragmentManager().beginTransaction();
                        ft2.replace(R.id.fragment_search, new BlankFragment());
                        ft2.replace(R.id.fragment_option_buttons, new BlankFragment());
                        ft2.commit();

                        // Auto-setup by adding the user's email address onto the edit text.
                        emailAddress = email;

                    }

                }).
                setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        // Leave nothing.
                        emailAddress = "";

                    }

                });

        // Create the AlertDialog object and return it
        return builder.create();

    }





    /** Use this to get string value of the selected user's email address from inbox */
    public static String getEmailAddress() {

        return emailAddress;

    }





    /** Nullifies the string value of the user's email address, Leaving search text blank. */
    public static void emptySearchTextboxFromMessenger() {

        emailAddress = "";

    }





}
