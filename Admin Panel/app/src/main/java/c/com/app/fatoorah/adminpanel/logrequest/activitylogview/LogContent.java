package c.com.app.fatoorah.adminpanel.logrequest.activitylogview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** A database class that shows the user's contents when logged-in or signed-up */
public class LogContent {

    /** Log content items. */
    public static final List<LogContentItem> ITEMS = new ArrayList<LogContentItem>();

    /** A map of log content by users. */
    public static final Map<String, LogContentItem> ITEM_MAP = new HashMap<String, LogContentItem>();

    private static final int COUNT = 25; //TODO: --> Make this item count somewhat dynamic.





    //TODO: Disable this dummy content item generator or think of something where you can get info from database.
    static {

        for (int i = 1; i <= COUNT; i++) {

            addItem(createDummyContentItem(i));

        }

    }





    private static void addItem(LogContentItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }





    /** Intended to generate dummy content for testing. */
    private static LogContentItem createDummyContentItem(int position) {

        return new LogContentItem("USER no. " + String.valueOf(position), "ItemDummy" + position + "@yahoo.com", makeDetails(position));

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }

        return builder.toString();

    }





    /** a piece of info contained within the log content item. */
    public static class LogContentItem {

        // TODO: Revise constructor.

        // Original log contents
        public final String id;
        public final String details; // --> For testing only.
        public final String email;
        public final String action;
        public final String time;
        public final String date;
        public final String platform; // --> Is this iOS or Android?

        /** For testing only. Quick display test of items in a list. */
        public LogContentItem(String id, String email, String details) {

            this.id = id;
            this.email = email;
            this.details = details;

            action = "action";
            time = "6:00pm";
            date = "12/29/1990";
            platform = "Android";

        }

        public LogContentItem(String id, String email, String action, String time, String date, String platform) {

            this.id = id;
            this.email = email;
            this.action = action;
            this.time = time;
            this.date = date;
            this.platform = platform;

            details = "NAH";

        }

        @Override
        public String toString() {

            return email;

        }

    }





}
