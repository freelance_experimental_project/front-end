package c.com.app.fatoorah.adminpanel.cashbackhistory;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import c.com.app.fatoorah.adminpanel.R;
import c.com.app.fatoorah.adminpanel.cashbackhistory.CashbackHistoryFragmentActivity.OnListFragmentInteractionListener;
import c.com.app.fatoorah.adminpanel.cashbackhistory.cashbackview.CashbackContent.CashbackItem;

import java.util.List;

/** Recycler adapter for loading cashback history. */
public class CashbackHistoryRecyclerViewAdapter extends RecyclerView.Adapter<CashbackHistoryRecyclerViewAdapter.ViewHolder> {





    private final List<CashbackItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public CashbackHistoryRecyclerViewAdapter(List<CashbackItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cashback_history_fragment_item, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // TODO: Revise variable name according to "CashbackItem" class.

        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).email); // --> TODO: Changed to "email."

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        // TODO: Revise variable name and check for content change accordingly. (see CashbackItem.class)
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public CashbackItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number_cashback);
            mContentView = (TextView) view.findViewById(R.id.content_cashback);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }

    }





}
