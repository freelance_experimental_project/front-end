package c.com.app.fatoorah.adminpanel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import c.com.app.fatoorah.adminpanel.ui.toolfragment.TestFragment;

/** This Main Frag Activity class is for testing purposes. */
public class MainFragActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_frag_activity);

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, TestFragment.newInstance())
                    .commitNow();

        }

    }

}
