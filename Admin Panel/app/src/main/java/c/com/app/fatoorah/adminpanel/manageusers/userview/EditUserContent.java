package c.com.app.fatoorah.adminpanel.manageusers.userview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** A database class that shows the user's contents when logged-in or signed-up */
public class EditUserContent {

    /** Log content items. */
    public static final List<UserContentItem> ITEMS = new ArrayList<UserContentItem>();

    /** A map of log content by users. */
    public static final Map<String, UserContentItem> ITEM_MAP = new HashMap<String, UserContentItem>();

    private static final int COUNT = 25; //TODO: --> Make this item count somewhat dynamic.





    //TODO: Disable this dummy content item generator or think of something where you can get info from database.
    static {

        for (int i = 1; i <= COUNT; i++) {

            addItem(createDummyContentItem(i));

        }

    }





    private static void addItem(UserContentItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }





    /** Intended to generate dummy content for testing. */
    private static UserContentItem createDummyContentItem(int position) {

        return new UserContentItem("USER no. " + String.valueOf(position), "EditItemDummy" + position + "@yahoo.com", makeDetails(position));

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }

        return builder.toString();

    }





    /** a piece of info contained within the log content item. */
    public static class UserContentItem {

        // TODO: Revise constructor.

        // Original log contents
        public final String id;
        public final String details; // --> For testing only.
        public final String email;
        public final String action;
        public final String time;
        public final String date;
        public final String platform; // --> Is this iOS or Android?

        /** For testing only. Quick display test of items in a list. */
        public UserContentItem(String id, String email, String details) {

            this.id = id;
            this.email = email;
            this.details = details;

            action = "NAH";
            time = "NAH";
            date = "NAH";
            platform = "NAH"; // --> Avoids returning uninitialized variable during testing.

        }

        public UserContentItem(String id, String email, String action, String time, String date, String platform) {

            this.id = id;
            this.email = email;
            this.action = action;
            this.time = time;
            this.date = date;
            this.platform = platform;

            details = "NAH";

        }

        @Override
        public String toString() {

            return email;

        }

    }





}
