package c.com.app.fatoorah.adminpanel.receiptmanagement.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.ImageView;

import c.com.app.fatoorah.adminpanel.R;

public class ReceiptPreviewDialog extends DialogFragment {





    private ImageView receipt;
    public int imageResource = R.drawable.ic_launcher_background; // --> By default.





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // For custom layout...
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Receipt (TODO: FIgure it out how to fetch image receipt of the user and paste it to the dialog's image)
        try {

            receipt = (ImageView) getActivity().findViewById(R.id.image_receipt_zoomed);
            receipt.setImageResource(imageResource);

        } catch(NullPointerException e) {

            System.out.println("NO IMAGE RESOURCE FOUND.\n");
            e.printStackTrace();

        }

        builder.setView(inflater.inflate(R.layout.receipt_preview_dialog, null))
                .setNeutralButton("CLOSE", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {

                        // Nothing.

                    }

                });

        // Create the AlertDialog object and return it
        return builder.create();

    }





}
