package com.test.ddimalanta.checkoutcopycat.receipt;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.test.ddimalanta.checkoutcopycat.MainActivity;
import com.test.ddimalanta.checkoutcopycat.R;

public class UploadReceiptActivity extends AppCompatActivity {





    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_receipt);

    }





    public void TakeReceiptByPicture(View v) {

        // Or, you can make logic in uploading pictures here.

        ReceiptEvent.uploadReceipt(); // --> Triggers or flagged as "upload complete."
        Toast.makeText(UploadReceiptActivity.this, "Upload complete.", Toast.LENGTH_SHORT).show();
//        startActivity(new Intent(UploadReceiptActivity.this, MainActivity.class));
        finish(); // --> This closes the current activity without adding additional memory.

    }





}
