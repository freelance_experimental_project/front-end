package c.com.app.fatoorah.enduserpanel.offers.offerlist.content;

import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Contains specific imageSource related to a given offer selected. */
public class ProductOfferContent {





    /** An array of offer items. */
    public static final List<OfferItem> ITEMS = new ArrayList<OfferItem>();
    public static final List<OfferItem> ITEMS_WISHLIST = new ArrayList<OfferItem>();

    /** A map of offer items, by ID. */
    public static final Map<String, OfferItem> ITEM_MAP = new HashMap<String, OfferItem>();
    public static final Map<String, OfferItem> ITEM_MAP_WISHLIST = new HashMap<String, OfferItem>();

    private static final int COUNT = 25; //--> TODO: Can you figure it out how to check total no. of offers dynamically not static?

    /** Enables test display or not. */
    public static boolean testDisplayItem = true;





    static {

        // Add some sample items.
        if(testDisplayItem) {

            for (int i = 1; i <= COUNT; i++) {

                addItem(createDummyItem(i)); // --> TODO: Can you make display of real offer info in real-time???

            }

        }

    }





    /** Use this to add item from the real offer list. */
    public static void addItem(OfferItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }





    /** Use this to add item directly to WISHLIST (YOUR LIST). */
    public static void addItemToWishlist(OfferItem item) {

        ITEMS_WISHLIST.add(item);
        ITEM_MAP_WISHLIST.put(item.id, item);

    }





    public static void removeItemToWishlist(OfferItem item, int id) {

        // TODO: Error. (Index out of bounds!)

        /*

        02-25 12:02:56.979 11709-11709/c.com.app.fatoorah.enduserpanel E/AndroidRuntime:

        FATAL EXCEPTION: main
        java.lang.IndexOutOfBoundsException: Invalid index 7, size is 3

        at java.util.ArrayList.throwIndexOutOfBoundsException(ArrayList.java:251)
        at java.util.ArrayList.remove(ArrayList.java:399)
        at c.com.app.fatoorah.enduserpanel.offers.offerlist.category.ProductOfferContent.removeItemToWishlist(ProductOfferContent.java:96)
        at c.com.app.fatoorah.enduserpanel.offers.offerlist.OfferScreenItemRecyclerViewAdapter$1.onClick(OfferScreenItemRecyclerViewAdapter.java:92)

        * */

        ITEM_MAP_WISHLIST.remove(item);
        ITEMS_WISHLIST.remove(id); // --> Error here. How to avoid delayed delete???

        // Afterwards, need something to refresh the WISHLIST.

    }





    /** For quick display testing purposes. */
    private static OfferItem createDummyItem(int position) {

        return new OfferItem(String.valueOf(position), "Milk Magic " + position, "$999 Cash Back", "no url", 5);

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for (int i = 0; i < position; i++) {
            builder.append("\nMore imageSource information here.");
        }

        return builder.toString();

    }





    /** An offer item representing a piece of category. */
    public static class OfferItem {

        public final String id;
        public final String productName;
        public final String details; // --> Cash back value
        public final String imgProductUrl; // --> URL image source of the product

        private boolean isAdded = false; // --> Allows you to add on "YOUR LIST"

        private int multiply = 0; // --> Allows you to get no. of offers of the same kind.
        private final int max;

        public OfferItem(String id, String productName, String details, String imgProductUrl, int maxOffer) {

            this.id = id;
            this.productName = productName;
            this.details = details;
            this.imgProductUrl = imgProductUrl;

            isAdded = false;
            multiply = 0;
            max = maxOffer;

        }

        /** Tells if the selected offer is added to YOUR LIST. */
        public boolean isAdded() {

            return isAdded;

        }

        /** Set this flag if item added or not. */
        public void toggleAdd() {

            if(isAdded) {

                isAdded = false;

            } else if(!isAdded) {

                isAdded = true;

            }

        }

        public void raiseOfferQuantity() {

            multiply++;

            if(multiply >= max) multiply = max;

        }

        public void reduceOfferQuantity() {

            multiply--;

            if(multiply <= 0) multiply = 0;

        }

        public int getTotalAmountOfThisOffer() {

            return multiply;

        }

        @Override
        public String toString() {
            return productName;
        }

    }





}
