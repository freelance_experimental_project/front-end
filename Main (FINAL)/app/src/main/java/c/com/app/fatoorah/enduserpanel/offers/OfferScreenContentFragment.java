package c.com.app.fatoorah.enduserpanel.offers;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.fatoorah.enduserpanel.R;

/** Under OFFER menu screen. The main category menu of the offers. */
public class OfferScreenContentFragment extends Fragment {





    public OfferScreenContentFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_offer_screen_content, container, false);

        return view;
    }





}
