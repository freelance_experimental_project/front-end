package c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist;

/** Indicator that allows you check no. of offers you've saved. */
public class WishListCounter {





    private static int offersSaved = 0;





    public static int numberOfOffersSaved() {

        return offersSaved;

    }





    public static void raiseCount() {

        offersSaved++;

    }





    public static void deductCount() {

        offersSaved--;

        if(offersSaved <= 0) offersSaved = 0;

    }





}
