package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import c.com.app.fatoorah.enduserpanel.R;

/** A profile submenu that allows you to change language */
public class LanguageFragment extends Fragment {





    private RadioGroup rgLanguage;





    public LanguageFragment() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_profile_sub_language, container, false);

        rgLanguage = (RadioGroup) view.findViewById(R.id.radio_language);

        rgLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                // TODO: Do something to change language. Code is still in sample test phase only for preview.
                String value = ((RadioButton) view.findViewById(checkedId)).getText().toString();
                Toast.makeText((FragmentActivity)view.getContext(), "LANGUAGE SELECTED: " + value, Toast.LENGTH_SHORT).show(); // --> Test preview.

            }

        });

        return view;

    }





}
