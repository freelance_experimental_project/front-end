package c.com.app.fatoorah.enduserpanel.redeem.savedoffers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.OfferScreenHeaderFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent.OfferItem;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.OfferScreenWishlistItemFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.WishListCounter;

/** Generates list of offers available */
public class RedeemItemListRecyclerViewAdapter extends RecyclerView.Adapter<RedeemItemListRecyclerViewAdapter.ViewHolder> {

    private final List<OfferItem> mValues;
    private final RedeemItemListFragment.OnListFragmentInteractionListener mListener;





    public RedeemItemListRecyclerViewAdapter(List<OfferItem> items, RedeemItemListFragment.OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_redeem_wish_item, parent, false);

        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        // TODO: Need revision to fetch product name, info, and image per item of the list. See @ViewHolder class.

        holder.mItem = mValues.get(position);

        holder.mTvProductName.setText(mValues.get(position).productName);
        holder.mTvProductOffer.setText(mValues.get(position).details);
        holder.mTvOfferQuantity.setText(Integer.toString(mValues.get(position).getTotalAmountOfThisOffer()));

//        holder.mImgProduct.setImageURI(); // --> Temporary close for now. This is where to display image from url.

        final int pos = position;

        // Initiator when adding the first quantity.
        holder.mImgBackground.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mValues.get(position).raiseOfferQuantity();

                if(mValues.get(position).getTotalAmountOfThisOffer() > 0) {

                    holder.mBtnAddOfferQty.setVisibility(View.VISIBLE);
                    holder.mBtnReduceOfferQty.setVisibility(View.VISIBLE);
                    holder.mTvOfferQuantity.setVisibility(View.VISIBLE);

                }

                holder.mTvOfferQuantity.setText(Integer.toString(mValues.get(position).getTotalAmountOfThisOffer()));

            }

        });

        // INCREASE OFFER QUANTITY (TODO: Need something to refresh this list view when an item is removed.)
        holder.mBtnAddOfferQty.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mValues.get(position).raiseOfferQuantity();
                holder.mTvOfferQuantity.setText(Integer.toString(mValues.get(position).getTotalAmountOfThisOffer()));

            }

        });

        // DECREASE OFFER QUANTITY (TODO: Need something to refresh this list view when an item is removed.)
        holder.mBtnReduceOfferQty.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mValues.get(position).reduceOfferQuantity();
                holder.mTvOfferQuantity.setText(Integer.toString(mValues.get(position).getTotalAmountOfThisOffer()));

                if(mValues.get(position).getTotalAmountOfThisOffer() <= 0) {

                    holder.mBtnAddOfferQty.setVisibility(View.INVISIBLE);
                    holder.mBtnReduceOfferQty.setVisibility(View.INVISIBLE);
                    holder.mTvOfferQuantity.setVisibility(View.INVISIBLE);

                }

            }

        });

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

//                    FragmentManager fm = ((FragmentActivity)v.getContext()).getSupportFragmentManager();
//                    DialogFragment dialog = new PreviewOfferDialogFragment(mValues.get(position).productName, mValues.get(position).details);
//                    dialog.show(fm, "PREVIEW OFFER");

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        public final TextView mTvProductName;
        public final TextView mTvProductOffer;
        public final TextView mTvOfferQuantity;

        public final ImageView mImgProduct;
        public final ImageView mImgBackground; // --> As a button.

        public final ImageButton mBtnReduceOfferQty;
        public final ImageButton mBtnAddOfferQty;

        public OfferItem mItem;

        public ViewHolder(View view) {

            super(view);

            mView = view;

            mTvProductName = (TextView) view.findViewById(R.id.tv_offer_product);
            mTvProductOffer = (TextView) view.findViewById(R.id.tv_offer_rebate);
            mTvOfferQuantity = (TextView) view.findViewById(R.id.tv_multiply_counter);

            mImgProduct = (ImageView) view.findViewById(R.id.img_product);
            mImgBackground = (ImageView) view.findViewById(R.id.img_background);

            mBtnReduceOfferQty = (ImageButton) view.findViewById(R.id.btn_reduce_qty);
            mBtnAddOfferQty = (ImageButton) view.findViewById(R.id.btn_add_qty);

            // Hide
            mBtnAddOfferQty.setVisibility(View.INVISIBLE);
            mBtnReduceOfferQty.setVisibility(View.INVISIBLE);
            mTvOfferQuantity.setVisibility(View.INVISIBLE);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTvProductOffer.getText() + "'";
        }

    }





}
