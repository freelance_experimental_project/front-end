package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.settingssubmenu;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.dialog.settings.PrivacyNoticeFragment;
import c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.dialog.settings.TermsFragment;

/** Submenu under SETTINGS. Displays legal and preference document. */
public class LegalAndPreferencesFragment extends Fragment {





    private Button btnPrivacyNotice;
    private Button btnTermsOfService;





    public LegalAndPreferencesFragment() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_settings_sub_legal_and_preferences, container, false);

        btnPrivacyNotice = (Button) view.findViewById(R.id.btn_privacy_notice);
        btnTermsOfService = (Button) view.findViewById(R.id.btn_terms_of_service);

        // PRIVACY NOTICE
        btnPrivacyNotice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new PrivacyNoticeFragment();
                dialog.show(fm, "PRIVACY NOTICE");

            }

        });

        // TERMS OF SERVICE
        btnTermsOfService.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                DialogFragment dialog = new TermsFragment();
                dialog.show(fm, "TERMS OF SERVICE");

            }

        });

        return view;

    }





}
