package c.com.app.fatoorah.enduserpanel.redeem;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.redeem.camera.CameraFragment;

/**
 *
 * Under REDEEM screen. List of saved offers ready to be submitted. It allows
 * you to multiply amount of the same item.
 *
 * */
public class RedeemMultiplyQuantityFragment extends Fragment {





    private Button btnContinue;





    public RedeemMultiplyQuantityFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_redeem_wish_list_content, container, false);

        btnContinue = (Button) view.findViewById(R.id.btn_continue);

        btnContinue.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_redeem_and_upload, new CameraFragment());
                ft.commit();

            }

        });

        return view;

    }





}
