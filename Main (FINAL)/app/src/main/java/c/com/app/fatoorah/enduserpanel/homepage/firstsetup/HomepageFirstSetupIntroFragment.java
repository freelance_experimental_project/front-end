package c.com.app.fatoorah.enduserpanel.homepage.firstsetup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import c.com.app.fatoorah.enduserpanel.R;

public class HomepageFirstSetupIntroFragment extends Fragment {





    private Button btnNext;





    public HomepageFirstSetupIntroFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_homepage_first_setup_intro, container, false);

        btnNext = (Button) view.findViewById(R.id.btn_next);

        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_homepage_main_content, new HomepageFirstSetupHowFragment());
                ft.commit();

            }

        });

        return view;

    }





}
