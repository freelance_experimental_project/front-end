package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.settingssubmenu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import c.com.app.fatoorah.enduserpanel.R;

public class PushNotificationsFragment extends Fragment {

        Switch sthBonuses;
        Switch sthNewOffers;
        Switch sthSavings;
        Switch sthInStore;


        public PushNotificationsFragment() {

            // Required empty public constructor

        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            final View view = inflater.inflate(R.layout.fragment_settings_sub_push_notification, container, false);

            sthBonuses = (Switch) view.findViewById(R.id.switch_bonuses);
            sthNewOffers = (Switch) view.findViewById(R.id.switch_new_offer);
            sthSavings = (Switch) view.findViewById(R.id.switch_extra_savings);
            sthInStore = (Switch) view.findViewById(R.id.switch_in_store);

            // TODO: Do something here to allow users to change or manage notifications within the app.

            return view;

        }





}
