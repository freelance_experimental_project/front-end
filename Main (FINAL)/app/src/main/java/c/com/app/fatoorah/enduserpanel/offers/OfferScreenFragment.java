package c.com.app.fatoorah.enduserpanel.offers;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.fatoorah.enduserpanel.R;

/** A menu screen contains list of avaiable offers to choose from. */
public class OfferScreenFragment extends Fragment {





    public OfferScreenFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_offer_screen_main, container, false);

        return view;

    }





}
