package c.com.app.fatoorah.enduserpanel.redeem.camera;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.TestFragment3;
import c.com.app.fatoorah.enduserpanel.profile.FragmentProfile;

/** Fragment that demonstrates the receipt is uploaded */
public class ReceiptUploadedFragment extends Fragment {





    private Button btnViewAccount; // --> TODO: How to redirect to PROFILE screen and marked the PROFILE tab at the same time???





    public ReceiptUploadedFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_camera_receipt_uploaded, container, false);

        btnViewAccount = (Button) view.findViewById(R.id.btn_continue);

        btnViewAccount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_redeem_and_upload, new TestFragment3());
                ft.replace(R.id.fragment_main, new FragmentProfile());
                ft.commit();

            }

        });

        return view;

    }





}
