package c.com.app.fatoorah.enduserpanel.homepage.offerlist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.homepage.offerlist.HomepageOfferItemFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.enduserpanel.homepage.offerlist.offer.DummyContent.DummyItem;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent;

import java.util.List;

/** Generates list of offers available but only in HOMEPAGE only. */
public class HompeageOfferItemRecyclerViewAdapter extends RecyclerView.Adapter<HompeageOfferItemRecyclerViewAdapter.ViewHolder> {





    private final List<ProductOfferContent.OfferItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public HompeageOfferItemRecyclerViewAdapter(List<ProductOfferContent.OfferItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_homepage_offer, parent, false);

        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // TODO: Need revision to fetch product name, info, and image per item of the list. See @ViewHolder class.
        // TODO: Check for bugs.

        holder.mItem = mValues.get(position);
        holder.mTvProductName.setText(mValues.get(position).productName);
        holder.mTvProductOffer.setText(mValues.get(position).details);
//        holder.mImgProduct.setImageURI(); // --> Temporary close for now. This is where to display image from url.

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
//                    mListener.onListFragmentInteraction(holder.mItem);

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mTvProductName;
        public final TextView mTvProductOffer;
        public final ImageView mImgProduct;
        public ProductOfferContent.OfferItem mItem;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mTvProductName = (TextView) view.findViewById(R.id.tv_offer_product);
            mTvProductOffer = (TextView) view.findViewById(R.id.tv_offer_rebate);
            mImgProduct = (ImageView) view.findViewById(R.id.img_product);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTvProductOffer.getText() + "'";
        }

    }





}
