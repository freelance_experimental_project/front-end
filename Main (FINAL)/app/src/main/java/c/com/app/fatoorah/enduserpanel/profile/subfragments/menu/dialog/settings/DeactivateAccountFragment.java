package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu.dialog.settings;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import c.com.app.fatoorah.enduserpanel.R;

public class DeactivateAccountFragment extends DialogFragment {





    public Button btnY;
    public Button btnN;





    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_fragment_deactivate_account, null);

        builder.setView(view);

        btnY = (Button) ((View) view).findViewById(R.id.btn_yes);
        btnN = (Button) ((View) view).findViewById(R.id.btn_no);

        btnY.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Replace this test linecode with an actual code that deactivates account.
                System.exit(0);

            }

        });

        btnN.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                getDialog().dismiss();

            }

        });

        return builder.create();

    }





}
