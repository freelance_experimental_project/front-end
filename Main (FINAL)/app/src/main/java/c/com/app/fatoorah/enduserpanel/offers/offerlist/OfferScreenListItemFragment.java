package c.com.app.fatoorah.enduserpanel.offers.offerlist;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent.OfferItem;

/** A list of available offers given to a user. */
public class OfferScreenListItemFragment extends Fragment {





    /*

        TODO: Adapt selected category (e.g. poultry, pantry, dairy, etc.) using only one list from this class. (See NOTE)

        NOTE:
            This class/offer list shares all different kinds of categories from the filter.
        That way, it will make the search result easier and dynamic every time a user
        updates specific category and stores nearby from the FILTER dialog. Take that in
        mind. Can be used also as a list category base for "YOUR LIST" result for user's
        saved list of offers.

        Also, please check @ProductOfferContent class and @OfferScreenItemRecyclerViewAdapter class.

     */





    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;





    public OfferScreenListItemFragment() {

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_offer_screen_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {

            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            if (mColumnCount <= 1) {

                recyclerView.setLayoutManager(new LinearLayoutManager(context));

            } else {

                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));

            }

            recyclerView.setAdapter(new OfferScreenItemRecyclerViewAdapter(ProductOfferContent.ITEMS, mListener));

        }

        return view;

    }





//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnListFragmentInteractionListener) {
//            mListener = (OnListFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnListFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {

        void onListFragmentInteraction(OfferItem item);

    }





}
