package c.com.app.fatoorah.enduserpanel.homepage.offerlist.login;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.MainActivity;
import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.homepage.HomePageFragment;

/** A login screen under HOMEPAGE. */
public class HomepageLoginFragment extends Fragment { //--> TODO: Need email authentication here





    private Button btnLogin;
    private Button btnBack;

    private EditText etEmail;
    private EditText etPassword;

    private TextView tvSignup; // --> As a button
    private TextView tvError;





    public HomepageLoginFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_homepage_login, container, false);

        btnLogin = (Button) view.findViewById(R.id.btn_signin);
        btnBack = (Button) view.findViewById(R.id.btn_back);

        // TODO: See ID for email and password to avoid confusion. Needed to fetch info.
        etEmail = (EditText) view.findViewById(R.id.et_email); // --> NOTE: "et_email" is for login part. "et_email_address" is for changing email. See layout for comparisson.
        etPassword = (EditText) view.findViewById(R.id.et_password); // --> NOTE: "et_password" is for login part. "et_enter_password" and "et_re_enter_password" are for changing password. Same thing.

        tvSignup = (TextView) view.findViewById(R.id.tv_btn_sign_up);
        tvError = (TextView) view.findViewById(R.id.tv_error);

        // BACK BUTTON
        btnBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_homepage_main_content, new HomePageFragment());
                ft.commit();

            }

        });

        // LOGIN BUTTON
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                // TODO: Need to verify if either this account registered or empty. (Recommend: boolean)
                if(email.isEmpty() || password.isEmpty()) {

                    tvError.setText("Please fill email and password first.");
                    return;

                } else {

                    // If email and password are incorrect, set message follow by RETURN statement here.
                    // During testing phase, this productName here is ignored and skipped to the account
                    // menu.

                }

                // Correct login will proceed to main account menu.
                Context context = (FragmentActivity) view.getContext();
                ((FragmentActivity) view.getContext()).startActivity(new Intent(context, MainActivity.class));

            }

        });

        // SIGN-UP BUTTON
        tvSignup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_homepage_main_content, new HomepageSignupFragment());
                ft.commit();

            }

        });

        return view;

    }





}
