package c.com.app.fatoorah.enduserpanel.redeem.camera;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import c.com.app.fatoorah.enduserpanel.R;

/** Fragment that demonstrates the receipt being uploaded */
public class ReceiptUploadingFragment extends Fragment {





    // TODO: Make async system in uploading receipt. Like a loading screen that shows in progress.





    private ImageView dummyButton; // --> For testing.





    public ReceiptUploadingFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_camera_receipt_uploading, container, false);

        dummyButton = (ImageView) view.findViewById(R.id.dummy_button);

        dummyButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_redeem_and_upload, new ReceiptUploadedFragment());
                ft.commit();

            }

        });

        return view;

    }





}
