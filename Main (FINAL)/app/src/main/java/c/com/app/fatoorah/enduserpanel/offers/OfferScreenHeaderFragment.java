package c.com.app.fatoorah.enduserpanel.offers;


import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.TestFragment2;
import c.com.app.fatoorah.enduserpanel.offers.notifications.NotificationsFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.OfferSearchFilterFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.OfferScreenWishlistItemFragment;

/** Under OFFER menu screen. Contains action/control in how you display the items you're looking for. */
public class OfferScreenHeaderFragment extends Fragment {





    private static TextView tvOfferCount;

    private Button btnWishlist;
    private Button btnFilter;

    private ImageButton btnNotification;

    private EditText etSearch;





    public OfferScreenHeaderFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_offer_screen_header, container, false);

        tvOfferCount = (TextView) view.findViewById(R.id.tv_offer_count);

        btnWishlist = (Button) view.findViewById(R.id.btn_my_list);
        btnFilter = (Button) view.findViewById(R.id.btn_filter);
        btnNotification = (ImageButton) view.findViewById(R.id.btn_notifcation);

        etSearch = (EditText) view.findViewById(R.id.et_search_item_offer);

        // Custom tint for search textbox...
        Drawable drawable = getResources().getDrawable(R.drawable.outline_search_black_24dp);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.BLACK);
//        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_OVER);
        etSearch.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        // WISHLIST BUTTON
        btnWishlist.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_offer_content, new OfferScreenWishlistItemFragment());
//                ft.replace(R.id.fragment_offer_content, new TestFragment2());
                ft.commit();

            }

        });

        // FILTER BUTTON
        btnFilter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_offer_content, new OfferScreenContentFragment()); // --> Temporary for testing
                ft.commit();

                OfferSearchFilterFragment.getLayout().setVisibility(View.VISIBLE);

            }

        });

        // NOTIFICATION BUTTON
        btnNotification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_offer_content, new NotificationsFragment());
                ft.commit();

            }

        });

        return view;

    }





    public static void setListCount(int count) {

        tvOfferCount.setText(Integer.toString(count));

    }





}
