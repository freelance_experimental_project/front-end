package c.com.app.fatoorah.enduserpanel.offers.offerlist.filters;

import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.FilterRetailListFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.content.FilterContent.RetailerItem;

import java.util.List;

/** Holds the category and updates related to the list of retails. */
public class FilterRetailRecyclerViewAdapter extends RecyclerView.Adapter<FilterRetailRecyclerViewAdapter.ViewHolder> {





    private final List<RetailerItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public FilterRetailRecyclerViewAdapter(List<RetailerItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_offer_retails, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);
        holder.mRetailerName.setText(mValues.get(position).storeName); // --> TODO: Replace it with individual store name.

        // TODO: Need to bug fix in order to make only one selected item currently highlighted in green.
        try {

            holder.mView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if(holder.selected) {

                        holder.selected = false;

                    } else {

                        holder.selected = true;

                    }

                    if (null != mListener) {
                        // Notify the active callbacks interface (the activity, if the
                        // fragment is attached to one) that an item has been selected.
                        mListener.onListFragmentInteraction(holder.mItem);
                    }

                }

            });

            if(holder.selected) {

                holder.border.setBackground(ContextCompat.getDrawable((FragmentActivity)holder.mView.getContext(), R.drawable.border_green_filter));

            } else {

                holder.border.setBackground(ContextCompat.getDrawable((FragmentActivity)holder.mView.getContext(), R.drawable.border_black));

            }

        } catch(Throwable e) {

        }

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mRetailerName;
        public final ImageView mImgStoreBrand;
        public final LinearLayout border;
        public RetailerItem mItem;

        public boolean selected = false;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mRetailerName = (TextView) view.findViewById(R.id.tv_store_name);
            mImgStoreBrand = (ImageView) view.findViewById(R.id.img_store_brand);

            border = (LinearLayout) view.findViewById(R.id.filter_border);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mRetailerName.getText() + "'";
        }

    }





}
