package c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample category for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class FilterContent {





    // For list of stores/retailers...
    public static final List<RetailerItem> ITEMS_RETAILERS = new ArrayList<RetailerItem>();
    public static final Map<String, RetailerItem> ITEM_RETAILERS_MAP = new HashMap<String, RetailerItem>();

    // For list of categories...
    public static final List<CategoryItem> ITEMS_CATEGORY = new ArrayList<CategoryItem>();
    public static final Map<String, CategoryItem> ITEM_CATEGORY_MAP = new HashMap<String, CategoryItem>();

    private static final int COUNT = 25; // --> For testing. Can be specififed no. of stores available.





    static {

        for (int i = 1; i <= COUNT; i++) {

            addRetailerItem(createDummyRetailerItem(i)); // --> TODO: Replace with actual retail/category lists.

        }

    }





    private static void addRetailerItem(RetailerItem item) {

        ITEMS_RETAILERS.add(item);
        ITEM_RETAILERS_MAP.put(item.id, item);

    }





    public static void addCategoryItem(CategoryItem item) {

        ITEMS_CATEGORY.add(item);
        ITEM_CATEGORY_MAP.put(item.id, item);

    }





    private static RetailerItem createDummyRetailerItem(int position) {

        return new RetailerItem(String.valueOf(position), "Store " + position, "No image for now.");

    }





    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore imageSource information here.");
        }
        return builder.toString();

    }





    /** Retailer item contains very simple title. */
    public static class RetailerItem {

        public final String id;
        public final String storeName;
        public final String imageSource; // --> TODO: Either from website URL or attached from the drawable folder here.

        public RetailerItem(String id, String storeName, String imageSource) {

            this.id = id;
            this.storeName = storeName;
            this.imageSource = imageSource;

        }

        @Override
        public String toString() {
            return storeName;
        }

    }





    /** Category item contains very simple title. */
    public static class CategoryItem {

        public final String id;
        public final String category;

        public CategoryItem(String id, String category) {

            this.id = id;
            this.category = category;

        }

        public CategoryItem(int id, String category) {

            this.id = Integer.toString(id);
            this.category = category;

        }

        @Override
        public String toString() {
            return category;
        }

    }





}
