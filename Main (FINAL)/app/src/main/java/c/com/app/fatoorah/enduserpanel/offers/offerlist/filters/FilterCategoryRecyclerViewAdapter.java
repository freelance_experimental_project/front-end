package c.com.app.fatoorah.enduserpanel.offers.offerlist.filters;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.FilterCategoryListFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.content.FilterContent;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.content.FilterContent.CategoryItem;

/** Holds the category and updates related to the list of retails. */
public class FilterCategoryRecyclerViewAdapter extends RecyclerView.Adapter<FilterCategoryRecyclerViewAdapter.ViewHolder> {





    private final List<CategoryItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public FilterCategoryRecyclerViewAdapter(List<CategoryItem> items, OnListFragmentInteractionListener listener) {

        FilterContent.addCategoryItem(new CategoryItem(0, "All Categories"));
        FilterContent.addCategoryItem(new CategoryItem(1, "Popular"));
        FilterContent.addCategoryItem(new CategoryItem(2, "Bakery"));
        FilterContent.addCategoryItem(new CategoryItem(3, "Meat & Seafood"));
        FilterContent.addCategoryItem(new CategoryItem(4, "Personal Care"));
        FilterContent.addCategoryItem(new CategoryItem(5, "Beverages"));
        FilterContent.addCategoryItem(new CategoryItem(6, "Babies & Kids"));
        FilterContent.addCategoryItem(new CategoryItem(7, "Snacks & Sweets"));
        FilterContent.addCategoryItem(new CategoryItem(8, "Pantry"));
        FilterContent.addCategoryItem(new CategoryItem(9, "Dairy & Eggs"));
        FilterContent.addCategoryItem(new CategoryItem(10, "Home"));
        FilterContent.addCategoryItem(new CategoryItem(11, "Medicine  Health"));
        FilterContent.addCategoryItem(new CategoryItem(12, "Beer/Wine/Spirits"));
        FilterContent.addCategoryItem(new CategoryItem(13, "Pet"));

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_offer_category, parent, false);
        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);
        holder.mCategoryName.setText(mValues.get(position).category);

        // TODO: Need to bug fix in order to make only one selected item currently highlighted in green.
        try {

            holder.mView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if(holder.selected) {

                        holder.selected = false;

                    } else {

                        holder.selected = true;

                    }

                    if (null != mListener) {
                        // Notify the active callbacks interface (the activity, if the
                        // fragment is attached to one) that an item has been selected.
                        mListener.onListFragmentInteraction(holder.mItem);
                    }

                }

            });

            if(holder.selected) {

                holder.border.setBackground(ContextCompat.getDrawable((FragmentActivity)holder.mView.getContext(), R.drawable.border_green_filter));
//                holder.border.setBackgroundColor(Color.GREEN);

            } else {

                holder.border.setBackground(ContextCompat.getDrawable((FragmentActivity)holder.mView.getContext(), R.drawable.border_black));
//                holder.border.setBackgroundColor(Color.WHITE);

            }

        } catch(Throwable e) {

        }

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mCategoryName;
        public final ImageView mImgCategory;
        public final LinearLayout border;
        public CategoryItem mItem;

        public boolean selected = false;

        public ViewHolder(View view) {

            super(view);
            mView = view;
            mCategoryName = (TextView) view.findViewById(R.id.tv_category);
            mImgCategory = (ImageView) view.findViewById(R.id.img_category);

            border = (LinearLayout) view.findViewById(R.id.filter_border);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mCategoryName.getText() + "'";
        }

    }





}
