package c.com.app.fatoorah.enduserpanel.homepage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import c.com.app.fatoorah.enduserpanel.MainActivity;
import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.homepage.offerlist.login.HomepageLoginFragment;
import c.com.app.fatoorah.enduserpanel.offers.OfferScreenFragment;

public class HomePageFragment extends Fragment {





    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ImageButton btnFacebookLogin;
    private ImageButton btnEmailLogin;





    public HomePageFragment() {

        // Required empty public constructor

    }





    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomePageFragment.
     */
    public static HomePageFragment newInstance(String param1, String param2) {

        HomePageFragment fragment = new HomePageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }





    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_homepage, container, false);

        btnFacebookLogin = (ImageButton) view.findViewById(R.id.btn_facebook_login);
        btnEmailLogin = (ImageButton) view.findViewById(R.id.btn_email_login);

        // FACEBOOK LOGIN BUTTON
        btnFacebookLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                // TODO: Find a way to authenticate FB users before signing-in.
                ((FragmentActivity) view.getContext()).startActivity(new Intent((FragmentActivity)view.getContext(), MainActivity.class)); // --> Intended for quick testing of switching next activity.

            }

        });

        // NORMAL LOGIN BUTTON
        btnEmailLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick (View v){

                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();

                ft.replace(R.id.fragment_homepage_main_content, new HomepageLoginFragment());
                ft.commit();

            }

        });

        return view;

    }





//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }





    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);

    }





}
