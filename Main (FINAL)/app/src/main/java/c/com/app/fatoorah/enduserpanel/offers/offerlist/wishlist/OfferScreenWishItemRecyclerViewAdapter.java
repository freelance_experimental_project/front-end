package c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import c.com.app.fatoorah.enduserpanel.R;
import c.com.app.fatoorah.enduserpanel.offers.OfferScreenHeaderFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.filters.dialog.PreviewOfferDialogFragment;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.wishlist.OfferScreenWishlistItemFragment.OnListFragmentInteractionListener;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent;
import c.com.app.fatoorah.enduserpanel.offers.offerlist.content.ProductOfferContent.OfferItem;

/** Generates list of offers available */
public class OfferScreenWishItemRecyclerViewAdapter extends RecyclerView.Adapter<OfferScreenWishItemRecyclerViewAdapter.ViewHolder> {

    private final List<OfferItem> mValues;
    private final OnListFragmentInteractionListener mListener;





    public OfferScreenWishItemRecyclerViewAdapter(List<OfferItem> items, OnListFragmentInteractionListener listener) {

        mValues = items;
        mListener = listener;

    }





    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_offer_screen_item_wish, parent, false);

        return new ViewHolder(view);

    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        // TODO: Need revision to fetch product name, info, and image per item of the list. See @ViewHolder class.

        holder.mItem = mValues.get(position);
        holder.mTvProductName.setText(mValues.get(position).productName);
        holder.mTvProductOffer.setText(mValues.get(position).details);
//        holder.mImgProduct.setImageURI(); // --> Temporary close for now. This is where to display image from url.

        final int pos = position;

        // Check if user pressed the ADD button. (TODO: Need something to refresh this list view when an item is removed.)
        holder.mBtnRemoveOffer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Remove selected item from wish list.
                mValues.get(pos).toggleAdd();
                WishListCounter.deductCount();
                ProductOfferContent.removeItemToWishlist(holder.mItem, position);
                OfferScreenHeaderFragment.setListCount(WishListCounter.numberOfOffersSaved());

            }

        });

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (null != mListener) {

                }

            }

        });

    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }





    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        public final TextView mTvProductName;
        public final TextView mTvProductOffer;
        public final ImageView mImgProduct;

        public final ImageButton mBtnRemoveOffer; // --> Button to remove selected offer from YOUR LIST/WISHLIST.

        public OfferItem mItem;

        public ViewHolder(View view) {

            super(view);

            mView = view;

            mTvProductName = (TextView) view.findViewById(R.id.tv_offer_product);
            mTvProductOffer = (TextView) view.findViewById(R.id.tv_offer_rebate);
            mImgProduct = (ImageView) view.findViewById(R.id.img_product);

            mBtnRemoveOffer = (ImageButton) view.findViewById(R.id.btn_remove);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTvProductOffer.getText() + "'";
        }

    }





}
