package c.com.app.fatoorah.enduserpanel.profile.subfragments.menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import c.com.app.fatoorah.enduserpanel.R;

/** A submenu under PROFILE that changes your current email. */
public class ChangeEmailFragment extends Fragment {





    private EditText etEmail;
    private TextView tvEmail;

    private Button btnUpdateEmail;





    public ChangeEmailFragment() {

        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_profile_sub_change_email, container, false);

        etEmail = (EditText) view.findViewById(R.id.et_email_address);
        tvEmail = (TextView) view.findViewById(R.id.tv_email_address);

        btnUpdateEmail = (Button) view.findViewById(R.id.btn_update_email);

        btnUpdateEmail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: Put logic here on how to change email.

            }

        });

        return view;

    }





}
