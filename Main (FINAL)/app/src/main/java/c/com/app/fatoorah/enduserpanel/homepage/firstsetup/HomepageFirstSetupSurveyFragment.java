package c.com.app.fatoorah.enduserpanel.homepage.firstsetup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import c.com.app.fatoorah.enduserpanel.R;

public class HomepageFirstSetupSurveyFragment extends Fragment {





    private Button btnNext;

    // Member counter
    private ImageButton btnAdultUp;
    private ImageButton btnAdultDown;
    private ImageButton btnKidUp;
    private ImageButton btnKidDown;

    private TextView tvAdultCount;
    private TextView tvKidCount;

    private int adultCount = 0;
    private int kidCount = 0;

    // TODO: Change max count (kids and adults) here!
    private final int MAX = 10;





    public HomepageFirstSetupSurveyFragment() {

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_homepage_first_setup_survey, container, false);

        btnNext = (Button) view.findViewById(R.id.btn_next);

        btnAdultUp = (ImageButton) view.findViewById(R.id.btn_adult_up);
        btnAdultDown = (ImageButton) view.findViewById(R.id.btn_adult_down);
        btnKidUp = (ImageButton) view.findViewById(R.id.btn_kid_up);
        btnKidDown = (ImageButton) view.findViewById(R.id.btn_kid_down);

        tvAdultCount = (TextView) view.findViewById(R.id.tv_adult_count);
        tvKidCount = (TextView) view.findViewById(R.id.tv_kid_count);

        // - - - - - ADULT COUNT HERE - - - - -

        btnAdultUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                adultCount++;

                if(adultCount > MAX) adultCount = MAX;

                tvAdultCount.setText(Integer.toString(adultCount));

//                Toast.makeText(view.getContext(), "ADULT: " + adultCount, Toast.LENGTH_SHORT).show();

            }

        });

        btnAdultDown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                adultCount--;

                if(adultCount <= 0) adultCount = 0;

                tvAdultCount.setText(Integer.toString(adultCount));

//                Toast.makeText(view.getContext(), "ADULT: " + adultCount, Toast.LENGTH_SHORT).show();

            }

        });

        // - - - - - KID COUNT HERE - - - - -

        btnKidUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                kidCount++;

                if(kidCount > MAX) kidCount = MAX;

                tvKidCount.setText(Integer.toString(kidCount));

//                Toast.makeText(view.getContext(), "KID: " + kidCount, Toast.LENGTH_SHORT).show();

            }

        });

        btnKidDown.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                kidCount--;

                if(kidCount <= 0) kidCount = 0;

                tvKidCount.setText(Integer.toString(kidCount));

//                Toast.makeText(view.getContext(), "KID: " + kidCount, Toast.LENGTH_SHORT).show();

            }

        });

        // - - - - - NEXT BUTTON to BIRTHDAY - - - - -

        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO: DO something to fetch survey per user as part of registration here.

                // After survey, user's birthdate.
                FragmentManager fm = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragment_homepage_main_content, new HomepageFirstSetupBirthdayFragment());
                ft.commit();

            }

        });

        return view;

    }





}
